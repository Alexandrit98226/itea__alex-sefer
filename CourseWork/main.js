//Я не придумал, как сюда подвязать использование LocalStorage. Была идея сохранять поля формы, и при загрузке страницы заново (при условии наличия записи в LocalStorage)
//восстанавливать их, но моя реализация не позволяет это сделать. 

//Кнопка редактирования на данный момент просто удаляет элемент (то же самое, что и кнопка удаления)).
let bigOrderCounter = 1;
let drags = [];
var dragSrcEl = null;

function handleDragStart(e) {
    
    this.style.backgroundColor='#3bfc65';

    dragSrcEl = this;

    e.dataTransfer.effectAllowed = 'move';
    e.dataTransfer.setData('text/html', this.innerHTML);
}
function handleDragOver(e) {
    if (e.preventDefault) {
        e.preventDefault(); 
    }

    e.dataTransfer.dropEffect = 'move';  

    return false;
}

function handleDragEnter(e) {
    
    this.classList.add('over');
}

function handleDragLeave(e) {
    this.classList.remove('over'); 
}
function handleDrop(e) {
    this.style.backgroundColor="#8deba1";
    if (e.stopPropagation) {
        e.stopPropagation();
    }

    if (dragSrcEl != this) {
        dragSrcEl.innerHTML = this.innerHTML;
        this.innerHTML = e.dataTransfer.getData('text/html');
    }

    return false;
}

function handleDragEnd(e) {
    this.style.backgroundColor="#8deba1";

    [].forEach.call(drags, function (drag) {
        drag.classList.remove('over');
    });
    someObject.refreshField();

}
const dragAndDrop = () => {
    drags = document.querySelectorAll('.draggers');
    [].forEach.call(drags, function (col) {
        col.addEventListener('dragstart', handleDragStart, false);
        col.addEventListener('dragenter', handleDragEnter, false)
        col.addEventListener('dragover', handleDragOver, false);
        col.addEventListener('dragleave', handleDragLeave, false);
        col.addEventListener('drop', handleDrop, false);
        col.addEventListener('dragend', handleDragEnd, false);
    });

}

const createSubmit = () => {
    let arrOfSubmits = Array.from(addedComponents.querySelectorAll('input'));
    arrOfSubmits = arrOfSubmits.filter((item) => item.type == 'submit');
    if (arrOfSubmits.length != 0) {
        alert("На форме может присутствовать только один элемент типа 'Submit'. Чтобы создать новый элемент такого типа - удалите из формы предыдущий");
        return 0;
    }
    let ob = new InputSubmit();
}



const submitUsersForm = (e) => {
    e.preventDefault();
    let answers = {};
    let arrOfAnswer = Array.from(addedComponents.childNodes);
    arrOfAnswer = arrOfAnswer.map((item)=>item.childNodes[2]);
    arrOfAnswer = arrOfAnswer.filter((item) => item.type != 'submit');
    let required = arrOfAnswer.filter((item) => item.hasAttribute('required'));
    if (required.length != required.filter((item) => item.value != "").length) {
        alert('Вы заполнили не все необходимыe поля!');
        return 0;
    }
    for (let i = 0; i < arrOfAnswer.length; i++) {
        if ((arrOfAnswer[i].type == 'checkbox' || arrOfAnswer[i].type == 'radio') && !(arrOfAnswer[i].checked)) {
            continue;
        }
        console.log(arrOfAnswer[i].value,arrOfAnswer[i].name);
        Object.defineProperty(answers, arrOfAnswer[i].name, {
            value: arrOfAnswer[i].value,
            writable: true,
            enumerable: true,
            configurable: true
        });
    }
    localStorage.setItem('LastTime', JSON.stringify(answers));
    console.log(answers);
}

class MyForm {

    constructor() {
        this.target = '';
        this.name = '';
        this.autocomplete = false;
        this.method = 'POST';
        this.enctype = 'multipart/form-data';
        this.fields = [];
        this.newFieldOrder=[];
        
    }

    printOnScreen() {
        this.refresh();
        let fieldsContent = ``;
        if (this.fields.length != 0) {
            fieldsContent = this.fields.reduce((prev, curr) => prev + curr.getHTML(), ``)
        }
        formResults.innerText = `<form ${this.target == '' ? '' : 'action="' + this.target + '"'} ${this.name == '' ? '' : 'name="' + this.name + '"'} ${this.method == 'POST' ? 'method="post"' : 'method="get"'} enctype="${this.enctype}" autocomplete="${this.autocomplete}">
        ${fieldsContent}
        </form>`

    }

    refresh() {
        this.target = targetOfForm.value;
        this.name = nameOfForm.value;
        let radios = Array.from(document.querySelectorAll("input[name='_autocomplete']"))
        this.autocomplete = (radios.filter((i) => i.checked == true))[0].value;
        radios = Array.from(document.querySelectorAll("input[name='_method']"));
        this.method = (radios.filter((i) => i.checked == true))[0].value;
        radios = Array.from(document.querySelectorAll("input[name='_enctype']"));
        this.enctype = (radios.filter((i) => i.checked == true))[0].value;
    }

    addField(item) {
        if (item.type != 'radio') {
            if ((this.fields.filter((i) => i.name == item.name)).length != 0) {
                alert('Элемент с таким именем уже есть, измените поле Name');
                return 0;
            }
        }
        this.fields.push(item);
        elementResult.innerHTML = null;
        this.renderItems();
        
    }

    renderItems() {
        addedComponents.innerHTML = null;
        if (this.fields.length != 0) {
            for (let i = 0; i < this.fields.length; i++) {
                addedComponents.innerHTML += `<div class='draggers' draggable='true'>` + this.fields[i].getHTML() + `<button class='deleteButton' data-order='${this.fields[i].dataOrder}'></button>` + `<button class='editButton' data-order='${this.fields[i].dataOrder}'></button></div>`;
            }
            let buttons = Array.from(document.getElementsByClassName('deleteButton'));
            buttons.forEach((item) => item.addEventListener('click', (e) => { e.preventDefault(); this.removeItem(Number(e.target.dataset.order)) }));
            buttons = Array.from(document.getElementsByClassName('editButton'));
            buttons.forEach((item) => item.addEventListener('click', (e) => { e.preventDefault(); this.editItem(Number(e.target.dataset.order)) }));
        }
        if (document.getElementById('__getValue') != null) {
            __getValue.addEventListener('click', submitUsersForm);
        }
        dragAndDrop();
        
    }

    refreshField(){
        for(let i=0;i<addedComponents.childNodes.length;i++){
            this.newFieldOrder.push(Number(addedComponents.childNodes[i].lastChild.dataset.order));
        }
        let xField=[];
        for(let j=0;j<this.newFieldOrder.length;j++){
            xField.push(this.fields.filter((item)=>item.dataOrder==this.newFieldOrder[j])[0]);
        }        
        this.fields=xField.slice();
    }

    removeItem(index) {
        this.fields = this.fields.filter((item) => item.dataOrder != index);
        this.renderItems();
    }
    editItem(index) {
        this.removeItem(index);
    }
}
var someObject = new MyForm();

class baseInput {
    constructor(type) {
        this.disabled = false;
        this.name = '';
        this.className = '';
        this.readonly = false;
        this.required = false;
        this.type = type;
        this.label = '';
        this.dataOrder = 0;
        this.maxlength = 0;
        this.minlength = 0;
        this.render();
    }
    render() {
        elementResult.innerHTML = `
        <form id='__elementForm'>
        <label><input type='text' id='__labelOfElement' required> Label</label>
        <label><input type='text' id='__nameOfElement' required> Name</label>
        <label><input type='text' id='__classnameOfElement'> ClassName</label> 
        <label for='__disabled'>Disabled
        <input type='radio' name='__disabled' value='false' checked>No
        <input type='radio' name='__disabled' value='true' >Yes</label>
        <button id='__addElementToForm'> Добавить элемент в форму</button>
        </form>
        `
    }
}

class InputText extends baseInput {
    constructor() {
        super('text');
        this.placeholder = '';
    }
    render() {
        elementResult.innerHTML = `
        <form id='__elementForm'>
        <label><input type='text' id='__labelOfElement' required> Label</label>
        <label><input type='text' id='__nameOfElement' required> Name</label>
        <label><input type='text' id='__placeholderOfElement'> Placeholder</label>
        <label><input type='text' id='__classnameOfElement'> ClassName</label> 
        <label for='__required'>Required
        <span class='radioValue'><input type='radio' name='__required' value='no' checked>No</span>
        <input type='radio' name='__required' value='yes' >Yes</label>
        <label for='__disabled'>Disabled
        <input type='radio' name='__disabled' value='false' checked>No
        <input type='radio' name='__disabled' value='true' >Yes</label>
        <button id='__addElementToForm'> Добавить элемент в форму</button>
        </form>
        `
        let myFunc = this.changeNSave.bind(this);
        __addElementToForm.addEventListener('click', (e) => { e.preventDefault(); myFunc(); });


    }
    changeNSave() {

        this.placeholder = __placeholderOfElement.value;
        this.name = __nameOfElement.value;
        this.label = __labelOfElement.value;
        this.className = __classnameOfElement.value;
        this.dataOrder = bigOrderCounter++;
        let radios = Array.from(elementResult.querySelectorAll("input[name='__required']"));
        this.required = (radios.filter((i) => i.checked == true))[0].value;
        radios = Array.from(elementResult.querySelectorAll("input[name='__disabled']"));
        this.disabled = (radios.filter((i) => i.checked == true))[0].value;
        if (__nameOfElement.value == '' || __labelOfElement.value == '') {
            alert('Вы заполнили не все необходимые поля');
            return 0;
        }
        someObject.addField(this);
    }
    getHTML() {
        return (`<label for="${this.name}">${this.label}</label>
        <input type='${this.type}' name='${this.name}'${this.className == '' ? '' : 'class="' + this.className + '"'} ${this.placeholder == '' ? '' : 'placeholder="' + this.placeholder + '"'} ${this.required == 'yes' ? 'required' : ''} ${this.disabled == 'true' ? 'disabled' : ''} data-order='${this.dataOrder}'>`)
    }
}

class InputSubmit extends baseInput {
    constructor() {
        super('submit');
    }
    render() {
        elementResult.innerHTML = `
        <form id='__elementForm'>
        <label><input type='text' id='__valueOfElement' required> Value: </label>
        <label><input type='text' id='__classnameOfElement'> ClassName</label> 
 
        <button id='__addElementToForm'> Добавить элемент в форму</button>
        </form>
        `
        let myFunc = this.changeNSave.bind(this);
        __addElementToForm.addEventListener('click', (e) => { e.preventDefault(); myFunc(); });

    }
    changeNSave() {
        if (__valueOfElement.value == '') {
            alert('Вы заполнили не все поля, необходимые для создания элемента');
            return 0;
        }
        this.value = __valueOfElement.value;
        this.className = __classnameOfElement.value;
        this.dataOrder = bigOrderCounter++;
        this.id = '__getValue';
        someObject.addField(this);
    }
    getHTML() {
        return `<input type='submit' value='${this.value}'${this.className == '' ? '' : 'class="' + this.className + '"'}id='${this.id}'>
        `

    }
}

class InputPassword extends baseInput {
    constructor() {
        super('password');
    }
    render() {
        elementResult.innerHTML = `
        <form id='__elementForm'>
        <label><input type='text' id='__labelOfElement' required> Label</label>
        <label><input type='number' id='__maxlengthOfElement'> Maxlength</label>
        <label><input type='number' id='__minlengthOfElement'> Minlength</label>
        <label><input type='text' id='__nameOfElement' required> Name</label>
        <label><input type='text' id='__placeholderOfElement'> Placeholder</label>
        <label><input type='text' id='__classnameOfElement'> ClassName</label> 
        <label for='__required'>Required
        <span class='radioValue'><input type='radio' name='__required' value='no' checked>No</span>
        <input type='radio' name='__required' value='yes' >Yes</label>
        <label for='__disabled'>Disabled
        <input type='radio' name='__disabled' value='false' checked>No
        <input type='radio' name='__disabled' value='true' >Yes</label>
        <label for='__autocomplete'>Autocomplete
        <input type='radio' name='__autocomplete' value='on'>On
        <input type='radio' name='__autocomplete' value='off' checked >Off
        <input type='radio' name='__autocomplete' value='current-password' >Current-password
        <input type='radio' name='__autocomplete' value='new-password' >New-password</label>
        <button id='__addElementToForm'> Добавить элемент в форму</button>
        </form>
        `

        let myFunc = this.changeNSave.bind(this);
        __addElementToForm.addEventListener('click', (e) => { e.preventDefault(); myFunc(); });


    }
    changeNSave() {
        this.placeholder = (document.getElementById('__placeholderOfElement') == null ? '' : __placeholderOfElement.value);
        this.name = (__nameOfElement.value == "" ? exceptionNotAll() : __nameOfElement.value);
        this.label = __labelOfElement.value;
        this.className = __classnameOfElement.value;
        this.dataOrder = bigOrderCounter++;
        this.maxlength = Number(__maxlengthOfElement.value);
        this.minlength = Number(__minlengthOfElement.value);
        let radios = Array.from(elementResult.querySelectorAll("input[name='__required']"));
        this.required = (radios.filter((i) => i.checked == true))[0].value;
        radios = Array.from(elementResult.querySelectorAll("input[name='__disabled']"));
        this.disabled = (radios.filter((i) => i.checked == true))[0].value;
        radios = Array.from(elementResult.querySelectorAll("input[name='__autocomplete']"))
        this.autocomplete = (radios.filter((i) => i.checked == true))[0].value;
        if (__nameOfElement.value == '' || __labelOfElement.value == '') {
            alert('Вы заполнили не все необходимые поля');
            return 0;
        }
        someObject.addField(this);
    };
    getHTML() {
        return (`<label for="${this.name}">${this.label}</label>
        <input type='${this.type}'name='${this.name}'${this.className == '' ? '' : 'class="' + this.className + '"'} ${this.placeholder == '' ? '' : 'placeholder="' + this.placeholder + '"'} ${this.required == 'yes' ? 'required' : ''} 
        ${this.disabled == 'true' ? 'disabled' : ''} data-order='${this.dataOrder}' autocomplete='${this.autocomplete}' ${this.maxlength == 0 ? '' : 'maxlength="' + this.maxlength + '"'}${this.minlength == 0 ? '' : 'minlength="' + this.minlength + '"'}>`);
    };
}

class InputUrl extends InputText {
    constructor() {
        super();
        this.type = 'url';
    }
    render() {
        elementResult.innerHTML = null;
        super.render();
        let newLabel = document.createElement('label');
        let newInput = document.createElement('input');
        newInput.type = 'text';
        newInput.id = '__pattern'
        newInput.setAttribute('placeholder', 'Enter a regular expression');
        newLabel.setAttribute('for', '__pattern');
        newLabel.innerText = 'Pattern (optional)';
        newLabel.appendChild(newInput);
        __elementForm.insertBefore(newLabel, __elementForm[__elementForm.length - 1]);
    }
    getHTML() {
        let html = super.getHTML();
        if (this.pattern != '') {
            html = html.replace(/type='url'/gi, `type='url' pattern='${this.pattern}'`);
            return html;
        }
        else {
            return html;
        }

    }
    changeNSave() {
        this.pattern = __pattern.value;
        super.changeNSave();

    }
}

class InputColor extends baseInput {
    constructor() {
        super('color');
    }
    render() {
        elementResult.innerHTML = null;
        super.render();
        let newLabel = document.createElement('label');
        let newInput = document.createElement('input');
        newInput.type = 'text';
        newInput.id = '__value'
        newInput.setAttribute('placeholder', '#XXXXXX');
        newInput.setAttribute('minlength', '7');
        newInput.setAttribute('maxlength', '7');
        newLabel.setAttribute('for', '__value');
        newLabel.innerText = 'Start value (in HEX format )';
        newLabel.appendChild(newInput);
        __elementForm.insertBefore(newLabel, __elementForm.firstChild);

        let myFunc = this.changeNSave.bind(this);
        __addElementToForm.addEventListener('click', (e) => { e.preventDefault(); myFunc(); });
    }
    changeNSave() {
        this.value = __value.value;
        this.name = __nameOfElement.value;
        this.label = __labelOfElement.value;
        this.className = __classnameOfElement.value;
        this.dataOrder = bigOrderCounter++;
        let radios = Array.from(elementResult.querySelectorAll("input[name='__disabled']"));
        this.disabled = (radios.filter((i) => i.checked == true))[0].value;
        if (__nameOfElement.value == '' || __labelOfElement.value == '') {
            alert('Вы заполнили не все необходимые поля');
            return 0;
        }
        someObject.addField(this);
    }
    getHTML() {
        return `<label for="${this.name}">${this.label}</label>
        <input type='${this.type}'name='${this.name}' ${this.className == '' ? '' : 'class="' + this.className + '"'} ${this.value == '' ? '' : 'value="' + this.value + '"'} ${this.disabled == 'true' ? 'disabled' : ''} data-order='${this.dataOrder}'>
        `
    }
}

class InputEmail extends InputText {
    constructor() {
        super();
        this.type = 'email';
        this.mutiple = 'off';
    }
    render() {
        elementResult.innerHTML = null;
        super.render();
        let newLabel = document.createElement('label');
        let newInput = document.createElement('input');
        newInput.type = 'text';
        newInput.id = '__pattern'
        newInput.setAttribute('placeholder', 'Enter a regular expression');
        newLabel.setAttribute('for', '__pattern');
        newLabel.innerText = 'Pattern (optional)';
        newLabel.appendChild(newInput);
        __elementForm.insertBefore(newLabel, __elementForm[__elementForm.length - 1]);
        newLabel = document.createElement('label');
        newLabel.setAttribute('for', '__multiple');
        newLabel.innerText = 'Multiple mode';
        newLabel.innerHTML = `Multiple mode<input type='radio'name='__multiple' value='on'>On<input type='radio' name='__multiple' value='off' checked>Off`
        __elementForm.insertBefore(newLabel, __elementForm[__elementForm.length - 1]);
    }
    getHTML() {
        let html = super.getHTML();
        if (this.pattern != '') {
            html = html.replace(/type='email'/gi, `type='email' pattern='${this.pattern}'`);
        }
        if (this.multiple != 'off') {
            html = html.replace(/type='email'/gi, `type='email' multiple`);
        }
        return html

    }
    changeNSave() {
        this.pattern = __pattern.value;
        let radios = Array.from(document.querySelectorAll("input[name='__multiple']"))
        this.multiple = (radios.filter((i) => i.checked == true))[0].value;
        super.changeNSave();
    }
}

class InputFile extends InputText {
    constructor() {
        super();
        this.type = 'file';
        this.mutiple = 'off';
    }
    render() {
        super.render();
        elementResult.innerHTML = elementResult.innerHTML.replace(/Placeholder/, 'Accept');
        let newLabel = document.createElement('label');
        newLabel.setAttribute('for', '__multiple');
        newLabel.innerText = 'Multiple mode';
        newLabel.innerHTML = `Multiple mode<input type='radio'name='__multiple' value='on'>On<input type='radio' name='__multiple' value='off' checked>Off`
        __elementForm.insertBefore(newLabel, __elementForm[__elementForm.length - 1]);
        let myFunc = this.changeNSave.bind(this);
        __addElementToForm.addEventListener('click', (e) => { e.preventDefault(); myFunc(); });
    }
    getHTML() {
        let html = super.getHTML();
        console.log(html);
        if (this.placeholder != '') {
            html = html.replace(/placeholder/, 'accept');
        }
        if (this.multiple != 'off') {
            html = html.replace(/type='file'/gi, `type='file' multiple`);
        }
        return html

    }
    changeNSave() {
        let radios = Array.from(document.querySelectorAll("input[name='__multiple']"));
        this.multiple = (radios.filter((i) => i.checked == true))[0].value;
        super.changeNSave();
    }
}

class InputNumber extends baseInput {
    constructor() {
        super('number');
        this.step = 1;
    }
    render() {
        elementResult.innerHTML = `
    <form id='__elementForm'>
    <label><input type='text' id='__labelOfElement' required> Label</label>
    <label><input type='number' id='__maxlengthOfElement' required> Max</label>
    <label><input type='number' id='__minlengthOfElement' required=''> Min</label>
    <label><input type='number' id='__stepOfElement' > Enter step</label>
    <label><input type='text' id='__nameOfElement' required> Name</label>
    <label><input type='text' id='__placeholderOfElement'> Placeholder</label>
    <label><input type='text' id='__classnameOfElement'> ClassName</label> 
    <label for='__required'>Required
    <span class='radioValue'><input type='radio' name='__required' value='no' checked>No</span>
    <input type='radio' name='__required' value='yes' >Yes</label>
    <label for='__disabled'>Disabled
    <input type='radio' name='__disabled' value='false' checked>No
    <input type='radio' name='__disabled' value='true' >Yes</label>
    <button id='__addElementToForm'> Добавить элемент в форму</button>
    </form>
    `
        let myFunc = this.changeNSave.bind(this);
        __addElementToForm.addEventListener('click', (e) => { e.preventDefault(); myFunc(); });
    }
    changeNSave() {
        console.log();
        this.placeholder = (document.getElementById('__placeholderOfElement') == null ? '' : __placeholderOfElement.value);
        this.name = __nameOfElement.value;
        this.label = __labelOfElement.value;
        this.className = __classnameOfElement.value;
        this.dataOrder = bigOrderCounter++;
        this.maxlength = __maxlengthOfElement.type == 'time' || __maxlengthOfElement.type == 'date' || __maxlengthOfElement.type == 'datetime-local' ? __maxlengthOfElement.value : Number(__maxlengthOfElement.value);
        this.minlength = __minlengthOfElement.type == 'time' || __minlengthOfElement.type == 'date' || __minlengthOfElement.type == 'datetime-local' ? __minlengthOfElement.value : Number(__minlengthOfElement.value);
        let radios = Array.from(elementResult.querySelectorAll("input[name='__required']"));
        this.required = (radios.filter((i) => i.checked == true))[0].value;
        radios = Array.from(elementResult.querySelectorAll("input[name='__disabled']"));
        this.disabled = (radios.filter((i) => i.checked == true))[0].value;
        this.step = (document.getElementById('__stepOfElement') == null ? '' : __stepOfElement.value);
        if (__nameOfElement.value == '' || __labelOfElement.value == '') {
            alert('Вы заполнили не все необходимые поля');
            return 0;
        }
        someObject.addField(this);

    };
    getHTML() {
        return (`<label for="${this.name}">${this.label}</label>
        <input type='${this.type}'name='${this.name}'${this.className == '' ? '' : 'class="' + this.className + '"'} ${this.placeholder == '' ? '' : 'placeholder="' + this.placeholder + '"'} ${this.required == 'yes' ? 'required' : ''} 
        ${this.disabled == 'true' ? 'disabled' : ''} data-order='${this.dataOrder}' ${this.maxlength == 0 ? '' : 'max="' + this.maxlength + '"'}${this.minlength == 0 ? '' : 'min="' + this.minlength + '"'}${this.step != null ? 'step="' + this.step + '"' : ''}>`);
    };
}

class InputRange extends InputNumber {
    constructor() {
        super();
        this.type = 'range'
    }
    render() {
        super.render();
        let x = elementResult.innerHTML;
        x = x.replace(/Max/g, 'Max value');
        x = x.replace(/Min/g, 'Min value');
        elementResult.innerHTML = x;
        __elementForm.removeChild(__placeholderOfElement.closest('label'));

        let myFunc = this.changeNSave.bind(this);
        __addElementToForm.addEventListener('click', (e) => { e.preventDefault(); myFunc(); });


    }
    getHTML() {
        let html = super.getHTML();
        html = html.replace(/<input/g, `${this.minlength}<input`);
        html = html.replace(/>$/, `>${this.maxlength}`);
        return html;
    }
    changeNSave() {
        if (__maxlengthOfElement.value == '' || __minlengthOfElement.value == '') {
            alert('Вы заполнили не все необходимые поля');
            return 0;
        }
        super.changeNSave();
    }
}

class InputRadio extends InputText {
    constructor() {
        super();
        this.type = 'radio';
    }
    render() {
        elementResult.innerHTML = null;
        super.render();
        elementResult.innerHTML = elementResult.innerHTML.replace(/Placeholder/g, 'Value of RadioButton'); elementResult.innerHTML = elementResult.innerHTML.replace(/Required/g, 'Checked');
        let myFunc = this.changeNSave.bind(this);
        __addElementToForm.addEventListener('click', (e) => { e.preventDefault(); myFunc(); });
    }
    getHTML() {
        let html = super.getHTML();
        if (this.required == 'yes') {
            html = html.replace(/type='radio'/gi, `type='radio' checked`);
        }
        if (this.placeholder != '') {
            html = html.replace(/type='radio'/gi, `type='radio' value='${this.placeholder}'`);
            html = html.replace(/placeholder="\w+"/gi, ``);

            return html;
        }
        else {
            return html;
        }

    }
    changeNSave() {
        this.placeholder = __placeholderOfElement.value;
        if (__placeholderOfElement.value == '') {
            alert('Вы заполнили не все необходимые поля');
            return 0;
        }
        super.changeNSave();
    }
}

class InputCheckbox extends InputRadio {
    constructor() {
        super();
        this.type = 'checkbox';
    }
    render() {
        elementResult.innerHTML = null;
        super.render();
        elementResult.innerHTML = elementResult.innerHTML.replace(/Value of RadioButton/g, 'Value of checkbox');
        elementResult.innerHTML = elementResult.innerHTML.replace(/Required/g, 'Checked');
        let myFunc = this.changeNSave.bind(this);
        __addElementToForm.addEventListener('click', (e) => { e.preventDefault(); myFunc(); });
    }
    getHTML() {
        let html = super.getHTML();
        if (this.required == 'yes') {
            html = html.replace(/type='checkbox'/gi, `type='checkbox' checked`);
        }
        if (this.placeholder != '') {
            html = html.replace(/type='checkbox'/gi, `type='checkbox' value='${this.placeholder}'`);
            return html;
        }
        else {
            return html;
        }

    }
    changeNSave() {
        let radios = Array.from(elementResult.querySelectorAll("input[name='__required']"));
        this.required = (radios.filter((i) => i.checked == true))[0].value;
        this.placeholder = __placeholderOfElement.value;
        super.changeNSave();
    }
}

class InputTime extends InputNumber {
    constructor() {
        super();
        this.type = 'time';
    }
    render() {
        super.render();
        let x = elementResult.innerHTML;
        x = x.replace(/Max/g, 'Max value');
        x = x.replace(/Min/g, 'Min value');
        x = x.replace(/type="number"/, `type="time"`);
        x = x.replace(/type="number"/, `type="time"`)
        elementResult.innerHTML = x;

        __elementForm.removeChild(__placeholderOfElement.closest('label'));

        let myFunc = this.changeNSave.bind(this);
        __addElementToForm.addEventListener('click', (e) => { e.preventDefault(); myFunc(); });


    }
    getHTML() {
        return (super.getHTML());
    }
    changeNSave() {
        super.changeNSave();
    }
}
class InputDate extends InputNumber {
    constructor() {
        super();
        this.type = 'date';
        this.step = null;
    }
    render() {
        super.render();
        let x = elementResult.innerHTML;
        x = x.replace(/Max/g, 'Max value');
        x = x.replace(/Min/g, 'Min value');
        x = x.replace(/type="number"/, `type="date"`);
        x = x.replace(/type="number"/, `type="date"`);

        elementResult.innerHTML = x;
        __elementForm.removeChild(__placeholderOfElement.closest('label'));
        __elementForm.removeChild(__stepOfElement.closest('label'));

        let myFunc = this.changeNSave.bind(this);
        __addElementToForm.addEventListener('click', (e) => { e.preventDefault(); myFunc(); });
    }
    getHTML() {
        return (super.getHTML());
    }
    changeNSave() {
        if (__maxlengthOfElement.value < __minlengthOfElement.value) {
            alert("Максимальное значение не может быть равным, или меньше минимального");
            return 0;
        }
        super.changeNSave();
    }
}

class InputDatetimeLocal extends InputDate {
    constructor() {
        super();
        this.type = 'datetime-local';
        this.step = null;
    }
    render() {
        super.render();
        let x = elementResult.innerHTML;
        x = x.replace(/type="date"/, `type="datetime-local"`);
        x = x.replace(/type="date"/, `type="datetime-local"`);

        elementResult.innerHTML = x;
        let myFunc = this.changeNSave.bind(this);
        __addElementToForm.addEventListener('click', (e) => { e.preventDefault(); myFunc(); });
    }
    getHTML() {
        return (super.getHTML());
    }
    changeNSave() {
        super.changeNSave();
    }

}

class InputTextarea extends InputPassword {
    constructor() {
        super();
        this.type = 'textarea';
    }
    render() {
        super.render();
        let x = elementResult.innerHTML;
        x = x.replace(/Maxlength/, 'Cols');
        x = x.replace(/Minlength/, 'Rows');
        x = x.replace(/Autocomplete/, 'Wrap');
        x = x.replace(/Current-password/, 'Soft');
        x = x.replace(/New-password/, 'Hard');
        x = x.replace(/<input type="radio" name="__autocomplete" value="on">On/g, ' ');
        elementResult.innerHTML = x;
        __elementForm.removeChild(__placeholderOfElement.closest('label'));

        let myFunc = this.changeNSave.bind(this);
        __addElementToForm.addEventListener('click', (e) => { e.preventDefault(); myFunc(); });
    }
    changeNSave() {
        if (__maxlengthOfElement.value == '' || __minlengthOfElement.value == '') {
            alert('Вы заполнили не все необходимые поля');
            return 0;
        }
        if (Number(__minlengthOfElement.value < 1) || Number(__maxlengthOfElement.value < 1)) {
            alert('Значение атрибутов Cols и Raws не могут быть отрицательными');
            return 0;
        }
        super.changeNSave();
    };
    getHTML() {
        let html = super.getHTML();
        html = html.replace(/maxlength/, `cols`);
        html = html.replace(/minlength/, ` rows`);
        html = html.replace(/autocomplete/, ` wrap`);
        html = html.replace(/minlength/, ` rows`);
        html = html.replace(/minlength/, ` rows`);
        html = html.replace(/<input type='textarea'/, `<textarea `);
        html = html.replace(/>$/, `></textarea>`);
        html = html.replace(/current-password/, `soft`);
        html = html.replace(/new-password/, `hard`);
        return html;

    };
}

const getStarted = () => {
    getForm.addEventListener('click', () => someObject.printOnScreen());
    __text.addEventListener('click', () => new InputText());
    __textarea.addEventListener('click', () => new InputTextarea());
    __checkbox.addEventListener('click', () => new InputCheckbox());
    __radio.addEventListener('click', () => new InputRadio());
    __date.addEventListener('click', () => new InputDate());
    __dateTimeLocal.addEventListener('click', () => new InputDatetimeLocal());
    __color.addEventListener('click', () => new InputColor());
    __password.addEventListener('click', () => new InputPassword());
    __email.addEventListener('click', () => new InputEmail());
    __range.addEventListener('click', () => new InputRange());
    __file.addEventListener('click', () => new InputFile());
    __number.addEventListener('click', () => new InputNumber());
    __time.addEventListener('click', () => new InputTime());
    __url.addEventListener('click', () => new InputUrl());
    __submit.addEventListener('click', createSubmit);
}
window.addEventListener('DOMContentLoaded', getStarted);