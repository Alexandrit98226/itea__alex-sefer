/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
let Train = {
    name: '',
    speed: 0,
    passangers: 0,
    go: function () {
        this.speed=80;
        console.log('Поезд '+this.name+' везёт '+this.passangers+' пассажиров, со скоростью '+ this.speed+' км\\ч')
     },
    stop: function () {
        this.speed=0;
        console.log('Поезд '+this.name+' остановился. Скорость '+ this.speed+' км\\ч')
    },
    pickUpPass: function(a){
        if(typeof(a)===undefined){
            this.passangers+=5;
        }
        else{
            this.passangers+=a;
        }
    }
};
