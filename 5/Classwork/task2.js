/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/
let colors = {
  background: 'purple',
  color: 'white'
}

let colors2 = {
  background: 'red',
  color: 'yellow'
}

// function myCall( color ){
//   document.body.style.background = this.background;
//   document.body.style.color = color;
//   document.body.innerHTML='<h1>I know how binding works in JS</h1>'
// }
// myCall.call( colors, 'red' );


let myBind = function () {
  document.body.style.background = this.background;
  document.body.style.color = this.color;
}


const fn1 = myBind.bind( colors2 );
fn1();

function myApply(a){
  document.body.style.background =this.background;
  document.body.style.color =this.color;
  document.body.innerHTML='<h1>'+a+'</h1>';
}
let word=['header'];
myApply.apply(colors, word);