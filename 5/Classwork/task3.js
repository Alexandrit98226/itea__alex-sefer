/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода, status)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства


    Dog {
      name: '',
      breed: '',
      status: 'idle',

      changeProp: function( key, value ){...},
      showProps: function(){...}
    }

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }
*/
function Dog(name, breed, status){
  this.name=name;
  this.breed=breed;
  this.status=status;

  this.changeProp=function( key,  target){
    this[key]=target;
  }
  this.showProps=function(){
    for(key in this){
      console.log(key, this[key]);
    }
  }
}
let fox=new Dog('foxxie', 'toy', 'idle');
fox.changeProp('status','eating');
fox.showProps();