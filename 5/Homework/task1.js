/*

  Задание:


    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);


    {
      avatarUrl: 'https://...',
      addLike: function(){...}
    }
    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }

      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.
    <div id="CommentsFeed"></div>
*/

function Comment(name, text, avatar) {
  this.name = name;
  this.text = text;
  this.avatar = avatar;
  this.likes = 0;

}

Comment.prototype = {
  constructor: Comment,
  addLike: function () {
    this.likes += 5;
  }
}

var myComment1 = new Comment('Alex', 'Hi, my name is Alex', 'https://www.meme-arsenal.com/memes/09f1bb361592a9dd37d844c545f63864.jpg');
var myComment2 = new Comment('Andrew', 'Hi, my name is Andrew', 'https://www.meme-arsenal.com/memes/09f1bb361592a9dd37d844c545f63864.jpg');
var myComment3 = new Comment('Kate', 'Hi, my name is Kate', 'https://www.meme-arsenal.com/memes/09f1bb361592a9dd37d844c545f63864.jpg');
var myComment4 = new Comment('Iren', 'Hi, my name is Iren', 'https://www.meme-arsenal.com/memes/09f1bb361592a9dd37d844c545f63864.jpg');

myComment4.addLike();     //показана работа метода, который содержится в прототипе

var CommentsArray = [myComment1, myComment2, myComment3, myComment4];

function printComments(comments) {
  CommentsFeed.innerHTML = null;
  CommentsFeed.style.display = 'flex';
  this.print = function () {
    for (let i = 0; i < comments.length; i++) {

      let ob = document.createElement('div');
      let im = new Image(100, 100);
      let headFour = document.createElement('h4');
      let bq = document.createElement('p');
      
      im.src = comments[i].avatar;

      ob.style.margin = '100px';
      headFour.innerText = comments[i].name;
      bq.innerHTML = comments[i].text + '<br>' + 'Current likes: ' + comments[i].likes;
      ob.append(im, headFour, bq);
      CommentsFeed.appendChild(ob);
      
    }
  }
}

var j = new printComments(CommentsArray);
j.print();