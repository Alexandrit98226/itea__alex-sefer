/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

      новые элементы, чтобы анимация была каждый раз
*/

var OurSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
var currentPosition = 0;

window.onload = function () {
  console.log(document.getElementById('slider'));

  RenderImage(currentPosition);

  var buttonsSelector = document.querySelectorAll('button');
  buttonsSelector.forEach(function (btn) {
    btn.addEventListener('click', change_position);
  });
}
var RenderImage = function (position) {


  //clearing
  if (slider != null) {
    slider.innerHTML=null;
  }

  //creating and adding
  let ob = new Image(100, 100);
  ob.src = OurSliderImages[position];
  slider.appendChild(ob);

}

var change_position = function (a) {
  console.log("WORKED");

  if (a.target.id == "NextSilde") {
    console.log("NEXT");
    if (currentPosition == OurSliderImages.length-1) {
      currentPosition = -1;
    }
    currentPosition++;
    RenderImage(currentPosition);
    console.log(currentPosition);
  }
  else if (a.target.id == "PrevSilde") {
    if (currentPosition === 0) {
      currentPosition = 8;
    }
    currentPosition--;
    RenderImage(currentPosition);
  }

} 
