/*

    Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.
    
    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678
    
    
    */
function User(log, pas) {
    this.login = log;
    this.password = pas;
};

function logout() {
    localStorage.removeItem('user');
    location.reload();

}
document.addEventListener('DOMContentLoaded', () => {
    regis.addEventListener('click', regUser);
    if (localStorage.getItem('user') !== null) {
        reg.style.display = "none";
        document.body.innerHTML = `<h1>Welcome Back, ${JSON.parse(localStorage.getItem('user')).login}</h1><button id='_logout'>Выйти</button>`
        _logout.addEventListener('click', logout);
    }
});

function regUser() {
    location.reload();
    if (_login.value !== "" && _password.value !== "") {
        let ob = new User(_login.value, _password.value);
        localStorage.setItem('user', JSON.stringify(ob));
    }

}