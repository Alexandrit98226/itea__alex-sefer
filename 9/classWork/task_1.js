/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/
document.addEventListener('DOMContentLoaded', function () {
    console.log('worked');
    if (localStorage.getItem('back') !== null) {
        console.log(localStorage.getItem('back'));
        document.body.style.backgroundColor = "rgb(226,79,189)";
    } else {
        document.body.style.backgroundColor = 'red';
    }
});
const rInt = function (min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
const change = () => {
    let ob = {
        background: `rgb(${rInt(0, 255)},${rInt(0, 255)},${rInt(0, 255)})`
    }
    localStorage.setItem('back', ob.background);
    document.body.style.backgroundColor = localStorage.getItem('back')

}
changeColor.addEventListener('click', change);