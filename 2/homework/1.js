/*

  Задание 1.

  Написать скрипт который будет будет переключать вкладки по нажатию
  на кнопки в хедере.

  Главное условие - изменять файл HTML нельзя.

  Алгоритм:
    1. Выбрать каждую кнопку в шапке
      + бонус выбрать одним селектором

    2. Повесить кнопку онклик
        button1.onclick = function(event) {

        }
        + бонус: один обработчик на все три кнопки

    3. Написать функцию которая выбирает соответствующую вкладку
      и добавляет к ней класс active

    4. Написать функцию hideAllTabs которая прячет все вкладки.
      Удаляя класс active со всех вкладок

  Методы для работы:

    getElementById
    querySelector
    classList
    classList.add
    forEach
    onclick

    element.onclick = function(event) {
      // do stuff ...
    }

*/

// В задании номер 1 сказано, что вкладки должны "переключаться", а в задании 4 - что функция должна "закрывать все вкладки",
// Значит перед этим должно открыться больше, чем одна вкладка, если я всё правильно понял.

document.querySelectorAll('.showButton').forEach(function (item) {
  item.onclick = function (e) {

    //Раскомментируйте эту строку, чтобы вкладки "переключались". Иначе они просто открываются, по нажатию на соответствующую кнопку.

    // if(document.querySelector('.active')!=null){document.querySelector('.active').classList.remove('active');}
    
    document.querySelector('.tab[data-tab="' + item.dataset.tab + '"]').classList.add('active');
  }
});

//Я решил добавить такую же кнопку, которая будет закрывать все вкладки

let close_tab=function(){
  let node = document.querySelector('.active');
  if( node !== null){
     node.classList.remove('active');
  }
};

let ob = document.createElement('button');
ob.classList.add('closeButton');
ob.onclick = close_tab;
ob.innerText = "Hide all tabs";
document.getElementById('buttonContainer').appendChild(ob);
