
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>
    <button></button>
  </form>

  <form>
    <input />
    <button></button>
  </form>
  -> '{"name" : !"23123", "age": 15, "password": "*****" }'


*/
const goParse = (e) => {
  e.preventDefault();
  let ob = JSON.parse(__textToParse.value);

  for (key in ob) {
    result.innerHTML += `${key}: ${ob[key]}<br>`
  }

}
const goJson = function (e) {
  e.preventDefault();
  let ob = {};
  Array.from(__myForm.elements).forEach(function (item) {
    if (item.value !== '') {
      Object.defineProperty(ob, item.name, {
        value: item.value, enumerable: true,
        configurable: true
      });
    }
  });
  console.log(JSON.stringify(ob));
}
window.onload = () => {
  __enter.addEventListener('click', goJson);
  __parser.addEventListener('click', goParse);
}